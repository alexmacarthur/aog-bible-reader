'use strict';

const { dialogflow } = require('actions-on-google');
const request = require('request');

const app = dialogflow({ debug: true });

app.intent('read_verse', (conv, { book, verse, chapter }) => {
    return new Promise((resolve, reject) => {
        request(`https://bible-api.com/${book}+${chapter}:${verse}`, (error, response, body) => {

            let parsedBody = JSON.parse(body);

            console.log(parsedBody);
            console.log(error);

            if (error) {
                conv.close(`Sorry, something went wrong with that request.`);
                return reject();
            }

            conv.close(parsedBody.text);
            return resolve();
        });
    });
});

exports.handler = app;